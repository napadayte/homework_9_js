// 1.

let feature = document.querySelectorAll('.feature');

let feature1 = document.getElementsByClassName('feature');

feature.forEach(element => {
    element.style.textAlign = 'center';
});

// 2.

let h2Elements = document.getElementsByTagName('h2');

for (let i = 0; i < h2Elements.length; i++) {
    h2Elements[i].innerText = "Awesome feature";
}

// 3.

let featureTitle = document.querySelectorAll('.feature-title');

featureTitle.forEach(element => {
    element.innerText += "!";
});
