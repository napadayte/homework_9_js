### Теоретичні питання

1. Опишіть своїми словами що таке Document Object Model (DOM)

   Document Object Model (DOM) — це програмний інтерфейс для HTML і XML документів. Він представляє документ у вигляді дерева об'єктів, де кожен елемент документа є вузлом цього дерева. DOM дозволяє програмам на JavaScript та інших мовах динамічно маніпулювати вмістом, структурою та стилем документа. З допомогою DOM можна змінювати текст, атрибути, створювати, видаляти та змінювати елементи.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

   - innerHTML: повертає або встановлює HTML-розмітку, що міститься всередині елемента. Використовується для вставки HTML-коду.

     element.innerHTML = "<p>Hello, World!</p>";

     Перевага: дозволяє вставляти HTML-розмітку.

   - innerText: повертає або встановлює текстовий вміст елемента, ігноруючи будь-яку HTML-розмітку. Використовується для вставки тексту.

     element.innerText = "Hello, World!";

     Недолік: не дозволяє вставляти HTML-код.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

   Є кілька способів звернення до елементів сторінки за допомогою JavaScript:

   - getElementById: повертає елемент з унікальним ідентифікатором (ID).

     let element = document.getElementById("myId");


   - getElementsByClassName: повертає колекцію елементів, що мають вказаний клас.

     let elements = document.getElementsByClassName("myClass");


   - getElementsByTagName: повертає колекцію елементів з вказаним тегом.

     let elements = document.getElementsByTagName("div");


   - querySelector: повертає перший елемент, що відповідає CSS-селектору.

     let element = document.querySelector(".myClass");


   - querySelectorAll: повертає всі елементи, що відповідають CSS-селектору.

     let elements = document.querySelectorAll(".myClass");


   Кращий спосіб залежить від конкретного завдання. querySelector і querySelectorAll є більш універсальними і дозволяють використовувати CSS-селектори для пошуку елементів, що робить їх дуже гнучкими та зручними.

4. Яка різниця між nodeList та HTMLCollection?

   - nodeList: це колекція вузлів (node), яка може включати елементи, текстові вузли та інші типи вузлів. Може бути статичною або динамічною, залежно від методу, який її повертає.

     let nodes = document.querySelectorAll("div"); // Статичний nodeList
     let nodes = document.childNodes; // Динамічний nodeList


   - HTMLCollection: це колекція HTML-елементів. Завжди динамічна, тобто автоматично оновлюється, якщо документ змінюється.

     let elements = document.getElementsByTagName("div");


   Основні відмінності:
   - nodeList може включати будь-які типи вузлів, тоді як HTMLCollection включає тільки HTML-елементи.
   - nodeList може бути статичним або динамічним, в той час як HTMLCollection завжди динамічна.
   - nodeList дозволяє використовувати метод forEach, тоді як HTMLCollection не підтримує цей метод без попереднього перетворення на масив.


// Приклад перетворення HTMLCollection на масив для використання forEach
let elements = document.getElementsByTagName("div");
Array.from(elements).forEach(element => {
    console.log(element);
});
